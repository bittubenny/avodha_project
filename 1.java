import java.util.Scanner;

public class Main{

   public static void main(String[] args){

       Scanner sc = new Scanner(System.in);
       int[] num = new int[3];
       
       for (int i = 0 ; i <= 2 ; i++){
           System.out.println("Enter your " + (i + 1) +" number");
           num[i] = sc.nextInt();
       }
       
      System.out.println((num[0] < num[1]) ? (num[0] < num[2] ? num[0] +", (1st number) is smallest number" : num[2] + ", (3rd) is smallest number") : ((num[1] < num[2]) ? num[1] +", (2nd) is smallest number" : num[2] + ", (3rd) is smallest number")) ;
   }
}

