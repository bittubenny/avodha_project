import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String reverse="",numStr;
        int num ;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a number");
        num = sc.nextInt();
        numStr = String.valueOf(num);
        int length = (int)(Math.log10(num)+1);

        for(int i = 1 ; i <= length ; i++){

            reverse += (num%10)+"";
            num=num/10;
        }

        if (numStr.equals(reverse)){
            System.out.println("it is Palindrome");
        }
        else {
            System.out.println("not palindrome");
        }

    }
}


