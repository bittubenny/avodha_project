import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a number");

        int num = sc.nextInt();
        double result  = 1;
        for ( int i = 1 ; i <= num ; i++){
            result *= i;
        }

        System.out.println(result);

    }
}

