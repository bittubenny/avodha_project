import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int[] arr = new int[5];
        int sum=0;

        System.out.println("Please enter 5 elements :");

        for (int i = 0 ; i < 5 ; i++)
            arr[i] = sc.nextInt();

        for (int x : arr)
            sum +=x;

        System.out.println("sum = " + sum);
    }

}


