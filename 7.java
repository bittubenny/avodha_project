import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int num ;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a number");
        num = sc.nextInt();

        for(int i = 1 ; i <= 10 ; i++)
            System.out.println(i + "X" + num + " = " + i*num);
    }
}


